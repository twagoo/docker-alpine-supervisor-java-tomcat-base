#!/bin/bash

if [ "${HEALTHCHECK_TEST_URL_HTTP}" ]; then
	curl -s --fail "${HEALTHCHECK_TEST_URL_HTTP}" > /dev/null || exit 1
else
	echo "Warning: no HTTP test URL is set for Tomcat health check"
fi

if [ "${HEALTHCHECK_TEST_URL_HTTPS}" ]; then
	curl -s --fail --insecure "${HEALTHCHECK_TEST_URL_HTTPS}" > /dev/null || exit 1
else
	echo "Warning: no HTTPS test URL is set for Tomcat health check"
fi

exit 0